# resource gr
variable "resource_group_name" {
  type = string
}

variable "location" {
  type = string
}

# vnet
variable "virtual_network_name" {
  type = string
}

variable "address_space" {
  type = list(string)
}


# subnet
variable "subnet_name" {
  type = string
}

variable "subnet_prefix" {
  type = string
}
