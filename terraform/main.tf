terraform {
  required_providers {
    azurerm = "=2.56.0"
  }
}

provider "azurerm" {
  features {}
}

module "RG" {
  source = "./modules/ResourceGroup"
  resource_group_name = var.resource_group_name
  location = var.location
}

module "Vnet" {
  source = "./modules/VirtualNetwork"
  virtual_network_name = var.virtual_network_name
  address_space = var.address_space
  vnet_location = var.location
  resource_group_name = module.RG.rg-name
}

module "subnet" {
  source = "./modules/Subnets"
  subnet_name = var.subnet_name
  resource_group_name = module.RG.rg-name
  virtual_network_name = module.Vnet.vnet-name
  subnet_prefix = var.subnet_prefix
}

module "nsg" {
  source = "./modules/NetworkSG"
  location = var.location
  resource_group_name = module.RG.rg-name
}

module "publicIP" {
  source = "./modules/PublicIP"
  location = var.location
  resource_group_name = module.RG.rg-name
}

module "NIC" {
  source = "./modules/NetworkInterface"
  location = var.location
  resource_group_name = module.RG.rg-name
  subnet_id = module.subnet.subnet_id
}

module "vm" {
  source = "./modules/VirtualMachine"
  location = var.location
  resource_group_name = module.RG.rg-name
  nic_id = module.NIC.nic_id
}

# module "webapp" {
#   source = "./modules/AZAppService"
#   location = var.location
#   resource_group_name = var.resource_group_name
#   subnet_id = module.subnet.subnet_id
#   vnet_id = module.Vnet.vnet_id
# }
