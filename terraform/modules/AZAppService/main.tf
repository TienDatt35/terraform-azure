resource "azurerm_service_plan" "appservice_plan" {
  name                = "appservice-plan"
  location            = var.location
  resource_group_name = var.resource_group_name
  os_type             = "Linux"
  sku_name            = "P1v2"
}

resource "azurerm_linux_web_app" "linux_webapp" {
  name                = "web-app"
  location            = var.location
  resource_group_name = var.resource_group_name
  service_plan_id = azurerm_service_plan.appservice_plan.id

  app_settings = {
    WEBSITES_ENABLE_APP_SERVICE_STORAGE = "false"
    WEBSITE_DOCKER_CUSTOM_IMAGE_NAME    = "nginx"
  }

  identity {
    type = "SystemAssigned"
  }

  depends_on = [azurerm_virtual_network_integration.vni]
}

resource "azurerm_virtual_network_integration" "vni" {
  name                = "my-virtual-network-integration"
  location            = var.location
  resource_group_name = var.resource_group_name
  subnet_id           = var.subnet_id

  virtual_network_id  = var.vnet_id
  service_name        = azurerm_linux_web_app.linux_webapp.name
}

