resource "azurerm_virtual_machine" "vm" {
  name                  = "backend"
  location              = var.location
  resource_group_name   = var.resource_group_name
  network_interface_ids = [var.nic_id]
  vm_size               = "Standard_DS1_v2"

  storage_image_reference {
    id = "/subscriptions/<SUBSCRIPTION_ID>/resourceGroups/<RESOURCE_GROUP_NAME>/providers/Microsoft.Storage/storageAccounts/<STORAGE_ACCOUNT_NAME>/blobServices/default/containers/<CONTAINER_NAME>/blobs/<IMAGE_NAME>"
  }

  storage_os_disk {
    name              = "example-osdisk"
    caching           = "ReadWrite"
    create_option     = "FromImage"
    managed_disk_type = "Standard_LRS"
  }

  os_profile {
    computer_name  = "backend"
    admin_username = "admin"
    admin_password = "admin"
  }
}