variable "location" {
  type = string
}

variable "resource_group_name" {
  type = string
}

variable "nic_id" {
  type = string
}