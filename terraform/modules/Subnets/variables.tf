variable "subnet_name" {
  type = string
}

variable "subnet_prefix" {
  type = string
}

variable "resource_group_name" {
  type = string  
}

variable "virtual_network_name" {
  type = string  
}